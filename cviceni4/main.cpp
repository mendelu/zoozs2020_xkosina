#include <iostream>
using namespace std;
//Mějme elektroauta (ElectricCar) a nabíjeci stanice (PowerStation).
// Každá nabíjecí stanice má určitý maximální nabíjecí proud (maxCurrent) a kapacitu v Ah, kterou za hodinu provozu při daném proudu dokáže dobít (hourChargeAh).
// Každé auto má maximální kapacitu baterie v Ah(maxAh), aktuální stav nabití v Ah (availableAh) a maximální nabíjecí proud.
//Nabíjecí stanice má metodu chargerForHour(), které předáme ukazatel na auto a ona ho dobije, kolik za hodinu zvládne.
// Kolik je schopna dobít se vypočítá tak, že pokud lze auto nabíjet maximálním proudem, nabije se na hodnotu danou hourChargeAh, pokud je méně, tak se udělá zlomek carMaxCurrent/stationMaxCurrent * hourChargeAh.
//Všechny uvedené vlastnosti jsou povinné, jsou tedy vynuceny konstruktory.
// Pokud někdo zadá nesmyslnou hodnotu (záporný proud atp.), bude tato chyba zalogována do třídy ErrorLogger, která bude obsahovat statický seznam chyb.
// Seznam chyb je implementován jako text (string) do kterého přidáváme informace.
//V zadání chybí celá řada metod, doplňte je podle nutnosti. Nedělejte zbytečné metody.
class ErrorLogger{
   static string s_errors;
public:
   static void addError(string newError){
        s_errors+=newError+="\n";
    }
    static void printErrors(){
        cout<<s_errors<<endl;
    }
};
string ErrorLogger::s_errors="";
class ElectricCar{
    float m_maxAh,m_availableAh,m_maxCurrent;
public:
    ElectricCar(float maxAh, float availableAh, float maxCurrent)
    {
        if(maxAh<=0)
        {
            ErrorLogger::addError("ElectricCar: maxAh must be >0");
       m_maxAh=1;
        }else {


            m_maxAh = maxAh;
        }
        if(m_availableAh<=0)
        {
            ErrorLogger::addError("ElectricCar: available must be >0");
            m_availableAh=1;
        }else {


            m_availableAh=availableAh;
        }

        m_maxCurrent=maxCurrent;

    }
    float getMaxCurrent(){
        return m_maxCurrent;
    }
    void charge(float maxAh){
        if((m_availableAh + maxAh)< m_maxAh){
            m_availableAh+=maxAh;

        }else{
            m_availableAh=m_maxAh;
        }
    }
    void printInfo(){
        cout<<"Max:"<<m_maxAh<<endl;
        cout<<"Current:"<<m_availableAh<<endl;
    }
};
class PowerStation{
    float m_maxCurrent;
    float m_hourChargeAh;
public:
    PowerStation(float maxCurrent, float hourChargeAh){
m_maxCurrent=maxCurrent;
m_hourChargeAh=hourChargeAh;
    }
    void chargeForHour(ElectricCar*car){
        float charge=(car->getMaxCurrent()/m_maxCurrent)*m_hourChargeAh;
        car->charge(charge);
    }
};
int main() {
    PowerStation*ps1=new PowerStation(4,4);
    ElectricCar*skoda=new ElectricCar(-20,-10,3);
    ps1->chargeForHour(skoda);
    skoda->printInfo();

    ErrorLogger::printErrors();
    delete skoda;
delete ps1;
    return 0;
}
