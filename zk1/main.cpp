#include <iostream>
//Vytvořte jednoduchou třídu pro evidenci práce
//programátorů. U každého programátora budeme
//evidovat jméno, hodinovou mzdu, počet
//odpracovaných hodin a bonus za hodinu přesčasu.
//Zadání těchto hodnot bude vynuceno v konstruktoru,
//kromě počtu odpracovaných hodin, ten bude nastaven
//na nulu.
//Developer bude mít metodu pracuj(). Ta přidá zadaný
//počet hodin k jeho odpracovaným hodinám.
//Dále bude mít metodu konecMesice(). Ta se zavolá na
//konci měsíce a nastaví počet opracovaných hodin na
//nulu.
//Také bude mít metodu getMzda(). Tato metoda vrátí
//mzdu developera, která se spočítá jako
//hodinová mzda * počet odpracovaných hodin + bonus za
//přesčasy.
//Počítejme s tím, že obvyklá pracovní doba je 40 hodin.
//Za každou hodinu odpracovanou nad obvyklou pracovní
//dobu získá navíc programátor bonus (dle atributu bonus
//za hodinu přesčasu)
//Vytvořte metodu printInfo() pro vypsání všech informací
//na obrazovku.
//Rozšíření
//Pokud bude developer dělat příliš mnoho chyb v kódu,
//projeví se to na jeho platu. Vytvořte atribut, který bude
//evidovat počet chyb a také atribut stanovující velikost
//srážky ze mzdy za každou chybu. Upravte metodu
//getMzda() aby reflektovala tuto funkcionalitu.
//Když developer udělá chybu, zavolá se metoda
//evidujChybu().
using namespace std;

class Programator{
private:
    string m_jmeno;
    int m_hodinovaMzda;
    int m_pocetOdpracovanychHodin;
    int m_bonusZaPrescas;
    int m_pocetChyb;
    int m_srazkaZaChybu;

public:
    Programator(string jmeno, int hodinovaMzda, int bonusZaPrescas, int srazkaZaChybu){
        m_jmeno = jmeno;
        m_hodinovaMzda = hodinovaMzda;
        m_bonusZaPrescas = bonusZaPrescas;
        m_srazkaZaChybu = srazkaZaChybu;
        m_pocetOdpracovanychHodin = 0;
        m_pocetChyb = 0;
    }

    Programator(string jmeno){
        m_jmeno = jmeno;
        m_hodinovaMzda = 500;
        m_bonusZaPrescas = 200;
        m_srazkaZaChybu = 300;
        m_pocetOdpracovanychHodin = 0;
        m_pocetChyb = 0;
    }


    void pracuj(int kolikHodin){
        m_pocetOdpracovanychHodin += kolikHodin;
    }

    void konecMesice(){
        m_pocetOdpracovanychHodin = 0;
        m_pocetChyb = 0;
    }

    void printInfo(){
        cout << "Programator: " << m_jmeno << endl;
        cout << "Odpracoval: " << m_pocetOdpracovanychHodin << endl;
        cout << "Jeho hodinova mzda: " << m_hodinovaMzda << endl;
        cout << "Udelal chyb:" << m_pocetChyb << endl;
        cout << "== Mzda == " << endl;
        cout << "Bonus za prescasy: " << getBonusKeMzde() << endl;
        cout << "Srazka za chyby: " << getSrazkaZaChyby() << endl;
        cout << "Dostane zaplaceno:" << getMzda() << endl;
    }

    int getMzda(){
        return m_hodinovaMzda * m_pocetOdpracovanychHodin + getBonusKeMzde() - getSrazkaZaChyby();
    }

    void evidujChybu(){
        m_pocetChyb += 1;
    }

private:
    int getBonusKeMzde(){
        if(m_pocetOdpracovanychHodin > 40){
            return (m_pocetOdpracovanychHodin-40)*m_bonusZaPrescas;
        }
        return 0;
    }

    int getSrazkaZaChyby(){
        return m_pocetChyb * m_srazkaZaChybu;
    }
};

int main() {
    Programator* tom = new Programator("Tomas",500,1000,200);
    tom->pracuj(10);
    tom->pracuj(5);
    tom->pracuj(40);
    tom->evidujChybu();
    tom->evidujChybu();
    tom->evidujChybu();
    tom->printInfo();
    return 0;
}