#include <iostream>
using namespace std;
class ErrorLogger{
    static string s_errors;
public:
  static  void AddErrors(string newErrors){
      s_errors+=newErrors+="\n";
  }
  static  void printErrors(){
        cout << s_errors<<endl;
    }

};
string ErrorLogger::s_errors="";
class ElectricCar {
    float m_maxAh;
    float m_availableAh;
    float m_maxCurrent;
public:
    ElectricCar(float maxAh, float availableAh, float maxCurrent) {
      if(maxAh<=0){
          ErrorLogger::AddErrors("ElectricCar:maxAH must be >0");
          m_maxAh=1;
      }
        m_maxAh = maxAh;
        m_availableAh = availableAh;
        m_maxCurrent = maxCurrent;
    }

    float getMaxCurrent() {
        return m_maxCurrent;
    }

    void charger(float maxAhFromPowerStation) {
        if ((m_availableAh + maxAhFromPowerStation) < m_maxAh){
            m_availableAh=maxAhFromPowerStation;
    }else{
            m_availableAh=m_maxAh;
        }


}
void printInfo(){
        cout<<"Max"<<m_maxAh<<endl;
        cout<<"Current:"<<m_availableAh<<endl;
    }


};


class PowerStation{
    float m_maxCurrent;
    float m_hourChargerAh;
public:
    PowerStation(float maxCurrent,float hourChargeAh){
        m_maxCurrent=maxCurrent;
        m_hourChargerAh=hourChargeAh;
    }
    void chargeForHour(ElectricCar*car){
        float charge=(car->getMaxCurrent()/m_maxCurrent)*m_hourChargerAh;
        car->charger(charge) ;

    }

};

int main() {
    PowerStation*ps1=new PowerStation(4,4);
    ElectricCar*skoda=new  ElectricCar(20,10,2);
    ps1->chargeForHour(skoda);
    skoda->printInfo();
    ErrorLogger::printErrors();
    delete skoda;
   delete ps1;
    return 0;
}
