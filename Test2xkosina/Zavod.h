//
// Created by Jakub on 01.12.2020.
//

#ifndef TEST2XKOSINA_ZAVOD_H
#define TEST2XKOSINA_ZAVOD_H
#include "Zavod.h"
#include"Zavodnik.h"
#include"Vozik.h"
class Zavod {
void pridejZavodnika(Zavodnik*zavodnik,std::string jmeno);
void getPrumernouRychlost(Vozik*vozik, float rychlost);
void vypisZavodniky();
};


#endif //TEST2XKOSINA_ZAVOD_H
