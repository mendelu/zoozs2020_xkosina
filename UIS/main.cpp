#include <iostream>
using namespace std;

class Student{
    string m_jmeno;
    string m_bydliste;
    int m_rokNarozeni;
public:
    Student(string jmeno, string bydliste, int rokNarozeni){
        setJmeno(jmeno);
        setBydliste(bydliste);
        m_rokNarozeni = rokNarozeni;
    }
    Student(string jmeno, int rokNarozeni):Student(jmeno, "Není známo", rokNarozeni){
    }
    string getJmeno(){
        return m_jmeno;
    }
private:
    void setJmeno(string jmeno){
        if (jmeno != ""){
            m_jmeno = jmeno;
        } else {
            cout << "Student: jmeno nemuze byt prazdne" << endl;
            m_jmeno = "Není známo";
        }
    }
    void setBydliste(string bydliste){
        if (bydliste != ""){
            m_bydliste = bydliste;
        } else {
            cout << "Student: bydliste nemuze byt prazdne" << endl;
            m_bydliste = "Není známo";
        }
    }
};
class Kurz {
    string m_nazev;
    int m_cena;
public:
    Kurz(string nazev, int cena){
        setNazev( nazev);
        m_cena = cena;
    }
string getNazev(){
        return m_nazev;
    }
private:
void setNazev(string nazev){
        if (nazev != ""){
            m_nazev = nazev;
        } else {
            cout << "Kurz: nazev nemuze byt prazdne" << endl;
            m_nazev = "Není známo";
        }
    }
};
class Zapis{
    int m_znamka;
    Student*m_zapsanyStudent;
    Kurz* m_zapsanyKurz;
public:
    Zapis(Kurz*kurz, Student*student){
        setKurz(kurz);
        setStudent(student);
        m_znamka=5;
    }
    void setZnamka(int znamka){
        if((znamka>5) ||(znamka<1)){
            cout<<"Znamka musi byt na intervali<1,5>";
        }else{
            m_znamka=znamka;
        }

    }
    void  printInfo(){
cout<<"Znamka:"<<m_znamka<<endl;
if(m_zapsanyStudent!= nullptr){
    cout<<"Jmeno studenta:"<< m_zapsanyStudent->getJmeno()<<endl;
}
if(m_zapsanyKurz!= nullptr){
    cout<<"Nazev kurzu:"<< m_zapsanyKurz->getNazev()<<endl;
}
cout<<"Jmena studentu"<<m_zapsanyStudent->getJmeno()<<endl;
cout<<"Nazev predmetu"<<m_zapsanyKurz->getNazev()<<endl;
    }
private:
   void setKurz(Kurz*kurz){
        if(kurz!= nullptr){
            m_zapsanyKurz=kurz;
        }else{
            cout<<"Zapis: nelze ukládat prázdny kurz"<<endl;
            m_zapsanyKurz= nullptr;
        }
    }
    void setStudent(Student*student){
        if(student!= nullptr){
            m_zapsanyStudent=student;
        }else{
            cout<<"Zapis: nelze ukládat prázdny kurz"<<endl;
            m_zapsanyStudent= nullptr;
        }

    }
};


int main() {
Student * David=new Student("David","Brno",1990);
Kurz* anglictina=new Kurz("Zaklady AJ",1000);
Zapis*DavidDoAj=new Zapis(anglictina,David);
DavidDoAj->printInfo();
delete DavidDoAj;
delete anglictina;
delete David;
return 0;
}