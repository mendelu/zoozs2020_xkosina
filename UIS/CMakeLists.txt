cmake_minimum_required(VERSION 3.17)
project(UIS)

set(CMAKE_CXX_STANDARD 14)

add_executable(UIS main.cpp)