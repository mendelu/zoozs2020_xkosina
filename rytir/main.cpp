#include <iostream>
using namespace std;

class Brneni{
    int m_bonusObrany;
public:
    Brneni(int bonusObrany){
        m_bonusObrany = bonusObrany;
    }

    int getBonusObrany(){
        return m_bonusObrany;
    }
};

class Rytir{
    int m_obrana;
    Brneni* m_neseneBrneni;

public:
    Rytir(int obrana, int bonusBrneni){
        m_obrana = obrana;
        m_neseneBrneni = new Brneni(bonusBrneni);
    }

    ~Rytir(){
        delete m_neseneBrneni;
    }

    int getObrana(){
        if (m_neseneBrneni == nullptr){
            return m_obrana;
        } else {
            return m_obrana + m_neseneBrneni->getBonusObrany();
        }
    }
};


int main() {
    Rytir* artus = new Rytir(50, 20);

    cout << "Obrana: " << artus->getObrana() << endl;

    delete artus;
    return 0;
}